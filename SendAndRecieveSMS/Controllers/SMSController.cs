﻿using System;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Messaging;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Twilio.AspNet.Common;
using System.Threading.Tasks;
using System.Configuration;

namespace SendAndRecieveSMS.Controllers
{
    public class SMSController : TwilioController
    {
        //[HttpPost]
        // GET: Sms

        public TwiMLResult Index(Twilio.AspNet.Common.SmsRequest request)
        {
            var messagingResponse = new MessagingResponse();
            var message = new Message();
            //request.From = "1234567890";
            //request.To = "9876543210";
            //request.Body = "This is a test message";           

            //Call the async function to update the details to CRM
            CreateSMSRecordInCRM(request);

            messagingResponse.Message("This is a test response message from Prem. I got your message. Will look in to your issue soon! {" + request.From + "}");

            return TwiML(messagingResponse);
        }

        private async Task CreateSMSRecordInCRM(SmsRequest request)
        {
            await Task.Run(() =>
            {
                string from = request.From;
                string body = request.Body;
                string messageStatus = request.MessageStatus;
                string to = request.To;
                string fromCountry = request.FromCountry;
                string toCountry = request.ToCountry;

                if (from == null || to == null || body == null)
                {
                    return;
                }

                OrganizationServiceProxy _serviceProxy = null;
                IOrganizationService _service;

                Uri crmURI = new Uri("https://bloomdev.api.crm4.dynamics.com/XRMServices/2011/Organization.svc");
                //Here check for the To number, based on that we will change CRM uri.
                if (to == "+87238972837")// create record in WCR. If it is europian number then save the sms record in WCR.
                {
                    crmURI = new Uri("https://bloomdev.api.crm4.dynamics.com/XRMServices/2011/Organization.svc");
                }
                else if (to == "+76127621212")// create record in WCR2
                {
                    crmURI = new Uri("https://bloomdev.api.crm4.dynamics.com/XRMServices/2011/Organization.svc");
                }

                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = ConfigurationManager.AppSettings["Username"];
                clientCredentials.UserName.Password = ConfigurationManager.AppSettings["Pwd"];

                using (_serviceProxy = new OrganizationServiceProxy(crmURI, null, clientCredentials, null))
                {
                    Entity sms = new Entity("new_sms");

                    sms["new_senderid"] = from;
                    sms["new_recipientmobile"] = to;
                    sms["description"] = body;
                    sms["subject"] = "Inbound";
                    sms["new_direction"] = true;//setting the value to incoming

                    _service = (IOrganizationService)_serviceProxy;

                    _service.Create(sms);
                }
            });
        }
    }
}

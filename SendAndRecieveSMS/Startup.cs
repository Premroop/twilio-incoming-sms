﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SendAndRecieveSMS.Startup))]
namespace SendAndRecieveSMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
